#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Jean-Cloud'
SITENAME = 'Jean-Cloud'
SITEURL = 'https://jean-cloud.net'
FAVICON = "theme/images/favicon.png"

#PATH = '.'
STATIC_PATHS = ['static']

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

THEME = './themes/jean-cloud/'

# Blogroll
LINKS = (
        ('Le site internet', 'https://jean-cloud.net'),
        )

# Social widget
SOCIAL = (
        ('Social is for weak people', '#'),
        )

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

USE_FOLDER_AS_CATEGORY = True
DEFAULT_CATEGORY = 'Trucs'
DEFAULT_DATE = 'fs'

INDEX_SAVE_AS = 'blog_index.html'
