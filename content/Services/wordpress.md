Title: Hébergement de Wordpress

## Qu’est-ce que wordpress
[Wordpress](https://wordpress.org) est un système de gestion de
contenu très performant, sous licence libre, très utilisé et
très renommé auprès des néophytes en informatique qui veulent
publier leur site internet.

Il s’agit bien d’une application web (c’est à dire un site internet
avec plein de fonctionnalités) conçu pour créer votre site à vous !

Nous hébergeons cet outil chez Jean-Cloud pour vous permettre de publier
votre site web. Avec une petite particularité.

## Wordpress chez Jean-Cloud

Victime de son succès, Wordpress est sujet à de très nombreuses cyberattaques.
Pour pallier ce problème, une stratégie très utilisée consiste à garder
le site wordpress inaccessible au public, protégé par un mot de passe.

Mais si il est innaccessible, personne ne peut voir ce site ?

C’est vrai, mais nous allons copier ce site internet caché vers un lieu
accessible au public. Seule cette copie sera exposée aux pirates, auxquels elle
est bien plus résistante !

Voici un shéma explicatif :
![Schéma de l’hébergement wordpress sur Jean-Cloud]({static}/static/img/schema-wordpress.svg "Shéma Wordpress")


## Comment l’utiliser concrètement ?
La section suivante part du principe que vous voulez crer un site web qui a pour adresse `monsite.fr`
Vous aurez alors trois adresses à noter :

- `monsite.fr` qui est l’adresse publique (et à diffuser) de votre site. Elle affiche l’export statique de votre wordpress.
- `wordpress.monsite.fr` Qui est l’adresse de votre wordpress (protégé par un mot de passe par sécurité).
- `wordpress.monsite.fr/wp-admin` Qui est l’interface de modification de votre wordpress (que vous utiliserez pour modifier votre site)

### S’identifier
L’identification se fait à l’aide d’un nom et d’un mot de passe qui vous sont donné à l’inscription.
Il est fortement recommandé de les changer (n’oubliez pas de les changer également dans le plugin simple static).

Il faut s’identifier deux fois avec le même mot de passe… C’est peu pratique mais c’est standard… Et on peut dire à son navigateur de sauvegarder les mots de passes.

Il faut aller sur le tableau de bord et bidouiller ce que l’on veut. C’est censé être intuitif mais en vrai on n’a pas tous la même intuition. Mieux vaut demander à des amis d’expliquer comment ça fonctionne ou aller chercher des tutos sur internet :)

### Modifier le site
Les modifications passent par le bord de wordpress (qui est à l’adresse `wordpress.monsite.fr/wp-admin`) et y bidouiller ce que l’on veut.
C’est censé être intuitif mais en vrai on n’a pas tous la même intuition.
Donc il ne faut pas hésiter à demander à des amis d’expliquer comment ça fonctionne ou aller chercher des tutos sur internet :)

Les modifications sont visibles sur le site privé (à l’adresse `wordpress.monsite.fr`) mais pas encore pour le public !
### Publier le site internet

Une fois le worpress modifié, il faut le publier. On va pour cela cliquer sur le menu « WP2Static ».

Puis il faut cliquer sur « Start static site export »

Et c’est tout ! Si il y a une erreur, il faut essayer de la comprendre… Ou contacter Jean-Cloud ;)

## Limitations
Évidemment il y a des limites à utiliser wordpress de cette façon. Un grand nombre de plugins ne fonctionneront plus, en particulier ceux qui ajoutent de l’interaction entre les visiteureuses et votre site (commentaires, livre d’or, comptes…).
Demandez conseil à votre informaticien·ne ;)

## Considérations techniques à destination des hébergeureuses
Le site est protégé par HTTP simple auth qui est simple à mettre en place.
Les plugins wordpress suivants sont utiles :
- wp2static
- Autologin
