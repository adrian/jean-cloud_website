Title: Héberger votre service chez Jean-Cloud

Si vous êtes dans une de ces situations :

- Vous souhaitez un devis pour l’hébergement d’un site ou d’un service numérique par Jean-Cloud ?  
- Vous souhaitez négocier un partenariat pour l’hébergement d’un service ?

Mauvaise nouvelle… La maison ne mange pas de ce pain là !

## Mais alors… Pour qui est Jean-Cloud ?
Pour les rares élu·e·s qui satisferont ces critères :

- Avoir besoin d’un service numérique (Revoir la définition de « besoin », le coût écologique de Jean-Cloud reste non-négligeable)
- Avoir envie d’un petit site web, et être prêt·e à faire des compromis pour qu’il soit le plus écologique possible
- Avoir un projet écologique ou social que nos bénévoles ont envie de soutenir avec leur travail
- Accepter le fait que notre infrastructure est fragile et donc que les sites et services ne sont pas toujours accessibles

La prochaine étape sera de discuter de vos besoins et envies pour trouver le meilleur compromis.

## Combien coûte Jean-Cloud ?
Jean-Cloud héberge vos services gratuitement, s’il en a envie et comme il en a envie !

Vous faites des dons si vous en avez envie et comme vous en avez envie ;)

(On envoie un mail par an pour informer des avancées et rappeler qu’on a besoin d’argent)

## Pour combien de temps mon service est-il hébergé ?
Jusqu’à ce que l’équipe démissionne, passe sous un bus, soit victime d’une attaque informatique synchronisée sur l’ensemble de ses serveurs, ordinateurs et sauvegardes (et encore là on reviendra)…

Nous essayerons (quand c’est possible) de vous prévenir au moins un an à l’avance de l’arrêt d’un service et nous vous guiderons vers des hébergeurs similaires.

## Si je veux partir de Jean-Cloud ?
Aucun soucis ! Libre à vous de trouver mieux ailleurs. Nous vous enverrons alors les données nécessaires pour héberger votre service sans nous.
Attention, les services mutualisés (nuage et git) ne savent pas migrer un compte d’un serveur à un autre. Certaines fonctionnalités seront alors perdues ou nécessiteront des opérations manuelles.

