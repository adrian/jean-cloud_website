title: Gestion technique des services

- [Où sont hébergées les services ?]({filename}/Meta/hebergement.md)
- [Qui s’occupe des services ?]({filename}/Meta/engagement_sysadmin.md)
- [Comment sont gérées les sauvegardes ?]({filename}/Meta/politique_sauvegardes.md)
- [Historique des pannes et maintenances.]({filename}/Meta/journal_technique.md)
