Title: Jean-Cloud est-il vraiment écologique ?

Non ! C’est du numérique ! Nous vous invitons à taper « impacte écologique numérique » dans un moteur de recherche.

## Alors il vaut mieux ne pas utiliser Jean-Cloud ?
Oui ;)

La meilleur énergie c’est celle qu’on ne dépense pas… Il paraît que c’est ça la sobriété…

## Mais si j’ai vraiment besoin d’un site ou d’une application ?
Ça arrive même aux meilleur·e·s… On vit dans une société numérisée, dur de passer à côté… Dans ce cas pas de soucis, on vous héberge.

## Donc Jean-Cloud est plus écologique que les autres ?
La question est compliquée… Nous travaillons sur les axes suivants :

### Utiliser du matériel de récupération
Pour donner une seconde vie aux ordinateurs et s’assurer de les utiliser jusqu’à la corde.

C’est plus difficile que ça en a l’air et des fois on achète des disques durs…

### Sortir des datacenters
Pour éviter d’utiliser de la climatisation.

Petit revers : on participe à décentraliser la consommation électrique du web, ce qui n’est pas un choix très pertinent avec une électricité nucléaire.

### Minimiser la taille des sites web
On travaille avec vous pour vous proposer les outils les plus efficaces pour alléger votre site web.

Nous nous inspirons de cette démarche : <a href="https://solar.lowtechmagazine.com/fr/2018/09/how-to-build-a-low-tech-website/">site web lowtech</a>.

