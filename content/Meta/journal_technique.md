Title: Journal des maintenances et problèmes

## Note aux utilisateurices
Jean-cloud est lent et économe en ressources. Ce qui signifie qu’il va moins vite que ses concurrents rapides et gourmands. Pas d’inquiétude à avoir de ce côté là !  
Jean-cloud est également maintenu bénévolement, donc certains services peuvent tomber en panne plus souvent que si des gens étaient payés 35h à les surveiller…
Mais si jamais c’est plus lent que d’habitude. Ou en cas d’indisponibilité d’un service, n’hésitez pas à nous contacter par mail (ou autres moyens de communications pour les chanceu·se·s qui ont nos contacts persos) !

# Historique des grandes pannes
Nous ne maintenons plus de journal des pannes.
L’exercice est soit très coûteux en énergie, soit trop grossier pour être honnête…
On se retrouve quand un robot s’en occupera ;)
