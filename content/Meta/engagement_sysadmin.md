title: Engagement de Jean-Cloud pour le respect des informations personnelles

## Qui a accès aux données hébergées par Jean-Cloud ?
Sur l’infrastructure actuelle, chaque service est assigné à un·e administrateur·ice système, et elle·lui seul·e peut accéder aux données du service.

## Mes données sont-elles à l’abris des regards indiscrets ?
Les administrateur·ice·s système s’engagent à ne pas accéder à des données non publiques.  
En revanche, Jean-Cloud ne garantit pas que des gouvernements ou des pirates ne puisse pas exploiter des failles récentes pour s’infiltrer dans nos serveurs. Mais ces derniers sont maintenus à jour automatiquement ce qui limite fortement ce risque.
