title: Gestion des sauvegardes
Les sauvegardes des fichiers est actuellement très simpliste : l’outil rsync est utilisé pour faire une copie des données des serveurs sur un disque chiffré localisé ailleurs.
Étant actuellemnt hébergés sur des services a plutôt haute disponibilité, cette solution semble convenir pour le moment, l’objectif est d’abord de prévenir la perte de donnée en cas d’erreur de manipulation.
