title: Où sont hébergés les services Jean-Cloud ?

Nos services sont actuellement installés chez Hostinger et chez des particuliers :

### Datacenter distribué
Pour résoudre des problèmes de disponibilité, de redondance des données, de climatisation et de centralisation de l’information, nous redéployons des mini serveurs chez des particulier·e·s.
Ils seront utilisés pour servir des sites web et certaines applications que nous hébergeons.
