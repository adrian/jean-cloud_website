Title: Comment soutenir Jean-Cloud

Plusieurs solutions s’offrent à vous :

- Nous donner du liquide
- Faire [un don en ligne](https://www.payasso.fr/jean-cloud/dons) (rien ne vous oblige à y mettre votre vraie identité).
- Demander le NOUVEAU rib et faire un virement
- Faire un chèque à l’ordre de Jean-Cloud
- Tout autre don utile sera le bienvenu (légumes, ordinateurs portables pas trop vieux, un merci…)
- Reversez l’argent à une autre association stylée. Ça vous fait deux achats de conscience en un ! #efficacité
