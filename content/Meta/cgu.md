Title: Conditions Générales d’Utilisation

Préambule
---------

En utilisant ce service, vous acceptez d'être lié par les conditions
suivantes. Jean-Cloud se réserve le droit de mettre à jour et modifier
ces conditions de temps à autre.

Version courte (TL;DR comme on dit chez nous)
---------------------------------------------

Pour en faciliter la lecture, sans le blabla juridique, nous vous en
proposons ci-dessous une version compréhensible par un être humain
normal.

### On est sympathiques :

-   Votre contenu vous appartient (mais nous vous encourageons à le
publier sous licence libre)
-   Jean-Cloud n'exploitera pas vos données personnelles, sauf à fin de
statistiques internes (anonymisées) ou pour vous prévenir d'un
changement important sur le service
-   Jean-Cloud ne transmettra ni ne revendra vos données personnelles
(votre vie privée nous tient - vraiment - à cœur) ;

### Mais il ne faut pas nous prendre pour autant pour des chatons

- **Clause « La loi est la loi, et on ne veut pas finir en taule » :**
Vous devez respecter la loi (que celle-ci soit bien faite ou
idiote), sinon votre compte sera supprimé. De la même manière, un juge
peut nous demander légalement de lui fournir tout ce qui vous concerne
(données, logs de connexion futurs ou de l’année passé…). Les services
de Jean-Cloud hébergés en France sont soumis à la loi française.
-   **Clause « Merci d'être poli et respectueux avec ses voisins de
service » :** Vous devez respecter les autres utilisateurs en
faisant preuve de civisme et de politesse, sinon votre contenu,
voire votre compte, pourront être supprimés, sans négociation
-   **Clause « Si ça casse, on n'est pas obligé de réparer » :** en cas
d'interruptions des services, restez patient. Bien que ce soit peu
probable, des données peuvent être perdues donc faites des
sauvegardes.
-   **Clause « Si vous n'êtes pas contents, vous êtes invités à aller
voir ailleurs » :** Si le service ne vous convient pas, libre à vous
d'en trouver un équivalent (ou meilleur) ailleurs, ou de monter le
vôtre.
-   **Clause « Tout abus sera puni » :** Si un utilisateur abuse des
services et des ressources, son contenu ou son compte pourront être
supprimés sans avertissement ni négociation. Jean-Cloud reste seul
juge de cette notion « d'abus » dans le but de fournir le meilleur
service possible à l'ensemble de ses utilisateurs.
-   **Clause « Rien n'est éternel » :** Les services peuvent fermer
(faute de fonds pour les maintenir, par exemple), ils peuvent être
victimes d'intrusion (le « 100 % sécurisé » n'existe pas). Nous vous
encourageons donc à conserver une copie des données qui vous
importent, car Jean-Cloud ne saurait être tenu pour responsable de
leur hébergement sans limite de temps.

Version complète des conditions du service
--------------------------------------

### Généralités
L’utilisation du service se fait à vos propres risques. Le service est fourni tel quel.
Vous ne devez pas créer ou modifier un autre site afin de signifier faussement qu’il est associé avec ce service Jean-Cloud.
Les comptes ne peuvent être créés et utilisés que par des humains. Les comptes créés par les robots ou autres méthodes automatisées pourront être supprimés sans avertissement.
Vous êtes responsable de la sécurité de votre compte et de votre mot de passe.
Jean-Cloud ne peut pas et ne sera pas responsable de toutes pertes ou dommages résultant de votre non-respect de cette obligation de sécurité.
Vous êtes responsable de tout contenu affiché et de l’activité qui se produit sous votre compte.
Vous ne pouvez pas utiliser le service à des fins illégales ou non autorisées.
Vous ne devez pas transgresser les lois de votre pays.
Vous ne pouvez pas vendre, échanger, revendre, ou exploiter dans un but commercial non autorisé un compte du service utilisé.

La violation de l'un de ces accords entraînera la résiliation de votre
compte. Vous comprenez et acceptez que Jean-Cloud ne puisse être tenu
responsable pour les contenus publiés sur ce service.

Vous comprenez que la mise en ligne du service ainsi que de votre contenu implique une transmission (en clair ou chiffrée, suivant les services) sur divers réseaux.
Vous ne devez pas transmettre des vers, des virus ou tout autre code de nature malveillante.
Jean-Cloud ne garantit pas que
le service répondra à vos besoins spécifiques,
que le service sera ininterrompu ou exempte de bugs,
ou que les erreurs dans le service seront corrigées.
Vous comprenez et acceptez que Jean-Cloud ne puisse être tenu responsable de tous dommages directs, indirects, ou fortuits, comprenant les dommages pour perte de profits, de clientèle, d’accès, de données ou d’autres pertes intangibles (même si Jean-Cloud est informé de la possibilité de tels dommages) et qui résulteraient de :
l’utilisation ou de l’impossibilité d’utiliser le service ;
l’accès non autorisé ou altéré de la transmission des données ;
les déclarations ou les agissements d’un tiers sur le service ;
la résiliation de votre compte ;
toute autre question relative au service.
L’échec de Jean-Cloud à exercer ou à appliquer tout droit ou disposition des Conditions Générales d’Utilisation ne constitue pas une renonciation à ce droit ou à cette disposition. Les Conditions d’utilisation constituent l’intégralité de l’accord entre vous et Jean-Cloud et régissent votre utilisation du service, remplaçant tous les accords antérieurs entre vous et Jean-Cloud (y compris les versions précédentes des Conditions Générales d’Utilisation).
Les questions sur les conditions de service doivent être envoyées via le formulaire de contact.

### Modifications du service

Jean-Cloud se réserve le droit, à tout moment de modifier ou d’interrompre, temporairement ou définitivement, le service avec ou sans préavis.
Jean-Cloud ne sera pas responsable envers vous ou tout tiers pour toute modification, suspension ou interruption du service.

### Droit d'auteur sur le contenu

Vous ne pouvez pas envoyer, télécharger, publier sur un blog, distribuer, diffuser tout contenu illégal, diffamatoire, harcelant, abusif, frauduleux, contrefait, obscène ou autrement répréhensible.
Nous ne revendiquons aucun droit sur vos données : textes, images, son, vidéo, ou tout autre élément, que vous téléchargez ou transmettez depuis votre compte.
Nous n’utiliserons pas votre contenu pour un autre usage que de vous fournir le service.
Vous ne devez pas télécharger ou rendre disponible tout contenu qui porte atteinte aux droits de quelqu’un d’autre.
Nous nous réservons le droit de supprimer tout contenu nous paraissant non pertinent pour l’usage du service, selon notre seul jugement.
Nous pouvons, si nécessaire, supprimer ou empêcher la diffusion de tout contenu sur le service qui ne respecterait pas les présentes conditions.

### Édition et partage de données

Les fichiers que vous créez avec le service peuvent être - si vous le souhaitez - lus, copiés, utilisés et redistribués par des gens que vous connaissez ou non.
En rendant publiques vos données, vous reconnaissez et acceptez que toute personne utilisant ce site web puisse les consulter sans restrictions.
Mais le service peut également vous proposer la possibilité d’autoriser l’accès et le travail collaboratif sur ses documents de manière restreinte à un ou plusieurs autres utilisateurs.
Jean-Cloud ne peut être tenu responsable de tout problème résultant du partage ou de la publication de données entre utilisateurs.

### Résiliation

Jean-Cloud, à sa seule discrétion, a le droit de suspendre ou de
résilier votre compte et de refuser toute utilisation actuelle ou future
du service. Cette résiliation du service entraînera la désactivation de
l'accès à votre compte, et la restitution de tout le contenu. Jean-Cloud
se réserve le droit de refuser le service à n'importe qui pour n'importe
quelle raison à tout moment.

Jean-Cloud se réserve également le droit de résilier votre compte si
vous ne vous connectez pas à votre compte pour une période supérieure à
6 mois. Données personnelles

Conformément à l'article 34 de la loi « Informatique et Libertés »,
Jean-Cloud garantit à l'utilisateur un droit d'opposition, d'accès et de
rectification sur les données nominatives le concernant. L'utilisateur a
la possibilité d'exercer ce droit en utilisant le formulaire de contact
mis à sa disposition.

### Données à caractère personnel
Pour utiliser certains services de Jean-Cloud, vous devez créer un compte. Jean-Cloud demande certaines informations personnelles : une adresse e-mail valide et un mot de passe qui est utilisé pour protéger votre compte contre tout accès non autorisé. Les champs « Nom » et « Prénom » peuvent être requis pour le bon fonctionnement du logiciel, mais il n’est pas nécessaire qu’ils révèlent votre véritable identité.
Tout comme d’autres services en ligne, Jean-Cloud enregistre automatiquement certaines informations concernant votre utilisation du service telles que l’activité du compte (exemple : espace de stockage occupé, nombre d’entrées, mesures prises), les données affichées ou cliquées (exemple : liens, éléments de l’interface utilisateur), et d’autres informations pour vous identifier (exemple : type de navigateur, adresse IP, date et heure de l’accès, URL de référence).
Nous utilisons ces informations en interne pour améliorer l’interface utilisateur des services de Jean-Cloud et maintenir une expérience utilisateur cohérente et fiable.
Ces données ne sont ni vendues, ni transmises à des tiers à l’exception d’une demande légale et mandatée dans le cadre d’une enquête judiciaire.

### Contact
Contactez nous pour toute remarque ou demande grace au formulaire à l’adresse `https://jean-cloud.net#contact`
